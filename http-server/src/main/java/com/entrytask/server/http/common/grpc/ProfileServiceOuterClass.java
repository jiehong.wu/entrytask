// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ProfileService.proto

package com.entrytask.server.http.common.grpc;

public final class ProfileServiceOuterClass {
  private ProfileServiceOuterClass() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_entrytask_server_http_common_grpc_queryRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_entrytask_server_http_common_grpc_queryRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_entrytask_server_http_common_grpc_queryResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_entrytask_server_http_common_grpc_queryResponse_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_entrytask_server_http_common_grpc_pictureRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_entrytask_server_http_common_grpc_pictureRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_entrytask_server_http_common_grpc_pictureResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_entrytask_server_http_common_grpc_pictureResponse_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_entrytask_server_http_common_grpc_nicknameRequest_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_entrytask_server_http_common_grpc_nicknameRequest_fieldAccessorTable;
  static final com.google.protobuf.Descriptors.Descriptor
    internal_static_com_entrytask_server_http_common_grpc_nicknameResponse_descriptor;
  static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_com_entrytask_server_http_common_grpc_nicknameResponse_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\024ProfileService.proto\022%com.entrytask.se" +
      "rver.http.common.grpc\" \n\014queryRequest\022\020\n" +
      "\010username\030\001 \001(\t\"D\n\rqueryResponse\022\020\n\010user" +
      "name\030\001 \001(\t\022\020\n\010nickname\030\002 \001(\t\022\017\n\007picture\030" +
      "\003 \001(\t\"Z\n\016pictureRequest\022\020\n\010username\030\001 \001(" +
      "\t\022\023\n\013picturePath\030\002 \001(\t\022\014\n\004file\030\003 \001(\014\022\023\n\013" +
      "pictureName\030\004 \001(\t\",\n\017pictureResponse\022\014\n\004" +
      "code\030\001 \001(\005\022\013\n\003msg\030\002 \001(\t\"5\n\017nicknameReque" +
      "st\022\020\n\010username\030\001 \001(\t\022\020\n\010nickname\030\002 \001(\t\"-" +
      "\n\020nicknameResponse\022\014\n\004code\030\001 \001(\005\022\013\n\003msg\030" +
      "\002 \001(\t2\217\003\n\016ProfileService\022y\n\014queryProfile" +
      "\0223.com.entrytask.server.http.common.grpc" +
      ".queryRequest\0324.com.entrytask.server.htt" +
      "p.common.grpc.queryResponse\022~\n\ruploadPic" +
      "ture\0225.com.entrytask.server.http.common." +
      "grpc.pictureRequest\0326.com.entrytask.serv" +
      "er.http.common.grpc.pictureResponse\022\201\001\n\016" +
      "updateNickname\0226.com.entrytask.server.ht" +
      "tp.common.grpc.nicknameRequest\0327.com.ent" +
      "rytask.server.http.common.grpc.nicknameR" +
      "esponseB\002P\001b\006proto3"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        });
    internal_static_com_entrytask_server_http_common_grpc_queryRequest_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_com_entrytask_server_http_common_grpc_queryRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_entrytask_server_http_common_grpc_queryRequest_descriptor,
        new java.lang.String[] { "Username", });
    internal_static_com_entrytask_server_http_common_grpc_queryResponse_descriptor =
      getDescriptor().getMessageTypes().get(1);
    internal_static_com_entrytask_server_http_common_grpc_queryResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_entrytask_server_http_common_grpc_queryResponse_descriptor,
        new java.lang.String[] { "Username", "Nickname", "Picture", });
    internal_static_com_entrytask_server_http_common_grpc_pictureRequest_descriptor =
      getDescriptor().getMessageTypes().get(2);
    internal_static_com_entrytask_server_http_common_grpc_pictureRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_entrytask_server_http_common_grpc_pictureRequest_descriptor,
        new java.lang.String[] { "Username", "PicturePath", "File", "PictureName", });
    internal_static_com_entrytask_server_http_common_grpc_pictureResponse_descriptor =
      getDescriptor().getMessageTypes().get(3);
    internal_static_com_entrytask_server_http_common_grpc_pictureResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_entrytask_server_http_common_grpc_pictureResponse_descriptor,
        new java.lang.String[] { "Code", "Msg", });
    internal_static_com_entrytask_server_http_common_grpc_nicknameRequest_descriptor =
      getDescriptor().getMessageTypes().get(4);
    internal_static_com_entrytask_server_http_common_grpc_nicknameRequest_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_entrytask_server_http_common_grpc_nicknameRequest_descriptor,
        new java.lang.String[] { "Username", "Nickname", });
    internal_static_com_entrytask_server_http_common_grpc_nicknameResponse_descriptor =
      getDescriptor().getMessageTypes().get(5);
    internal_static_com_entrytask_server_http_common_grpc_nicknameResponse_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_com_entrytask_server_http_common_grpc_nicknameResponse_descriptor,
        new java.lang.String[] { "Code", "Msg", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
