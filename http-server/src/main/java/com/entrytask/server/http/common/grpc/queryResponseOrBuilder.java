// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ProfileService.proto

package com.entrytask.server.http.common.grpc;

public interface queryResponseOrBuilder extends
    // @@protoc_insertion_point(interface_extends:com.entrytask.server.http.common.grpc.queryResponse)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>string username = 1;</code>
   * @return The username.
   */
  java.lang.String getUsername();
  /**
   * <code>string username = 1;</code>
   * @return The bytes for username.
   */
  com.google.protobuf.ByteString
      getUsernameBytes();

  /**
   * <code>string nickname = 2;</code>
   * @return The nickname.
   */
  java.lang.String getNickname();
  /**
   * <code>string nickname = 2;</code>
   * @return The bytes for nickname.
   */
  com.google.protobuf.ByteString
      getNicknameBytes();

  /**
   * <code>string picture = 3;</code>
   * @return The picture.
   */
  java.lang.String getPicture();
  /**
   * <code>string picture = 3;</code>
   * @return The bytes for picture.
   */
  com.google.protobuf.ByteString
      getPictureBytes();
}
