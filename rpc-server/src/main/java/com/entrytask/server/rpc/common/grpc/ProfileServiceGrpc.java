package com.entrytask.server.rpc.common.grpc;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.23.0)",
    comments = "Source: ProfileService.proto")
public final class ProfileServiceGrpc {

  private ProfileServiceGrpc() {}

  public static final String SERVICE_NAME = "com.entrytask.server.rpc.common.grpc.ProfileService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.entrytask.server.rpc.common.grpc.queryRequest,
      com.entrytask.server.rpc.common.grpc.queryResponse> getQueryProfileMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "queryProfile",
      requestType = com.entrytask.server.rpc.common.grpc.queryRequest.class,
      responseType = com.entrytask.server.rpc.common.grpc.queryResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.entrytask.server.rpc.common.grpc.queryRequest,
      com.entrytask.server.rpc.common.grpc.queryResponse> getQueryProfileMethod() {
    io.grpc.MethodDescriptor<com.entrytask.server.rpc.common.grpc.queryRequest, com.entrytask.server.rpc.common.grpc.queryResponse> getQueryProfileMethod;
    if ((getQueryProfileMethod = ProfileServiceGrpc.getQueryProfileMethod) == null) {
      synchronized (ProfileServiceGrpc.class) {
        if ((getQueryProfileMethod = ProfileServiceGrpc.getQueryProfileMethod) == null) {
          ProfileServiceGrpc.getQueryProfileMethod = getQueryProfileMethod =
              io.grpc.MethodDescriptor.<com.entrytask.server.rpc.common.grpc.queryRequest, com.entrytask.server.rpc.common.grpc.queryResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "queryProfile"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.entrytask.server.rpc.common.grpc.queryRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.entrytask.server.rpc.common.grpc.queryResponse.getDefaultInstance()))
              .setSchemaDescriptor(new ProfileServiceMethodDescriptorSupplier("queryProfile"))
              .build();
        }
      }
    }
    return getQueryProfileMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.entrytask.server.rpc.common.grpc.pictureRequest,
      com.entrytask.server.rpc.common.grpc.pictureResponse> getUploadPictureMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "uploadPicture",
      requestType = com.entrytask.server.rpc.common.grpc.pictureRequest.class,
      responseType = com.entrytask.server.rpc.common.grpc.pictureResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.entrytask.server.rpc.common.grpc.pictureRequest,
      com.entrytask.server.rpc.common.grpc.pictureResponse> getUploadPictureMethod() {
    io.grpc.MethodDescriptor<com.entrytask.server.rpc.common.grpc.pictureRequest, com.entrytask.server.rpc.common.grpc.pictureResponse> getUploadPictureMethod;
    if ((getUploadPictureMethod = ProfileServiceGrpc.getUploadPictureMethod) == null) {
      synchronized (ProfileServiceGrpc.class) {
        if ((getUploadPictureMethod = ProfileServiceGrpc.getUploadPictureMethod) == null) {
          ProfileServiceGrpc.getUploadPictureMethod = getUploadPictureMethod =
              io.grpc.MethodDescriptor.<com.entrytask.server.rpc.common.grpc.pictureRequest, com.entrytask.server.rpc.common.grpc.pictureResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "uploadPicture"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.entrytask.server.rpc.common.grpc.pictureRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.entrytask.server.rpc.common.grpc.pictureResponse.getDefaultInstance()))
              .setSchemaDescriptor(new ProfileServiceMethodDescriptorSupplier("uploadPicture"))
              .build();
        }
      }
    }
    return getUploadPictureMethod;
  }

  private static volatile io.grpc.MethodDescriptor<com.entrytask.server.rpc.common.grpc.nicknameRequest,
      com.entrytask.server.rpc.common.grpc.nicknameResponse> getUpdateNicknameMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "updateNickname",
      requestType = com.entrytask.server.rpc.common.grpc.nicknameRequest.class,
      responseType = com.entrytask.server.rpc.common.grpc.nicknameResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.entrytask.server.rpc.common.grpc.nicknameRequest,
      com.entrytask.server.rpc.common.grpc.nicknameResponse> getUpdateNicknameMethod() {
    io.grpc.MethodDescriptor<com.entrytask.server.rpc.common.grpc.nicknameRequest, com.entrytask.server.rpc.common.grpc.nicknameResponse> getUpdateNicknameMethod;
    if ((getUpdateNicknameMethod = ProfileServiceGrpc.getUpdateNicknameMethod) == null) {
      synchronized (ProfileServiceGrpc.class) {
        if ((getUpdateNicknameMethod = ProfileServiceGrpc.getUpdateNicknameMethod) == null) {
          ProfileServiceGrpc.getUpdateNicknameMethod = getUpdateNicknameMethod =
              io.grpc.MethodDescriptor.<com.entrytask.server.rpc.common.grpc.nicknameRequest, com.entrytask.server.rpc.common.grpc.nicknameResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "updateNickname"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.entrytask.server.rpc.common.grpc.nicknameRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.entrytask.server.rpc.common.grpc.nicknameResponse.getDefaultInstance()))
              .setSchemaDescriptor(new ProfileServiceMethodDescriptorSupplier("updateNickname"))
              .build();
        }
      }
    }
    return getUpdateNicknameMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static ProfileServiceStub newStub(io.grpc.Channel channel) {
    return new ProfileServiceStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static ProfileServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new ProfileServiceBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static ProfileServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new ProfileServiceFutureStub(channel);
  }

  /**
   */
  public static abstract class ProfileServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void queryProfile(com.entrytask.server.rpc.common.grpc.queryRequest request,
        io.grpc.stub.StreamObserver<com.entrytask.server.rpc.common.grpc.queryResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getQueryProfileMethod(), responseObserver);
    }

    /**
     */
    public void uploadPicture(com.entrytask.server.rpc.common.grpc.pictureRequest request,
        io.grpc.stub.StreamObserver<com.entrytask.server.rpc.common.grpc.pictureResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getUploadPictureMethod(), responseObserver);
    }

    /**
     */
    public void updateNickname(com.entrytask.server.rpc.common.grpc.nicknameRequest request,
        io.grpc.stub.StreamObserver<com.entrytask.server.rpc.common.grpc.nicknameResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getUpdateNicknameMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getQueryProfileMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.entrytask.server.rpc.common.grpc.queryRequest,
                com.entrytask.server.rpc.common.grpc.queryResponse>(
                  this, METHODID_QUERY_PROFILE)))
          .addMethod(
            getUploadPictureMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.entrytask.server.rpc.common.grpc.pictureRequest,
                com.entrytask.server.rpc.common.grpc.pictureResponse>(
                  this, METHODID_UPLOAD_PICTURE)))
          .addMethod(
            getUpdateNicknameMethod(),
            asyncUnaryCall(
              new MethodHandlers<
                com.entrytask.server.rpc.common.grpc.nicknameRequest,
                com.entrytask.server.rpc.common.grpc.nicknameResponse>(
                  this, METHODID_UPDATE_NICKNAME)))
          .build();
    }
  }

  /**
   */
  public static final class ProfileServiceStub extends io.grpc.stub.AbstractStub<ProfileServiceStub> {
    private ProfileServiceStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ProfileServiceStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ProfileServiceStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ProfileServiceStub(channel, callOptions);
    }

    /**
     */
    public void queryProfile(com.entrytask.server.rpc.common.grpc.queryRequest request,
        io.grpc.stub.StreamObserver<com.entrytask.server.rpc.common.grpc.queryResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getQueryProfileMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void uploadPicture(com.entrytask.server.rpc.common.grpc.pictureRequest request,
        io.grpc.stub.StreamObserver<com.entrytask.server.rpc.common.grpc.pictureResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getUploadPictureMethod(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void updateNickname(com.entrytask.server.rpc.common.grpc.nicknameRequest request,
        io.grpc.stub.StreamObserver<com.entrytask.server.rpc.common.grpc.nicknameResponse> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getUpdateNicknameMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class ProfileServiceBlockingStub extends io.grpc.stub.AbstractStub<ProfileServiceBlockingStub> {
    private ProfileServiceBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ProfileServiceBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ProfileServiceBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ProfileServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.entrytask.server.rpc.common.grpc.queryResponse queryProfile(com.entrytask.server.rpc.common.grpc.queryRequest request) {
      return blockingUnaryCall(
          getChannel(), getQueryProfileMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.entrytask.server.rpc.common.grpc.pictureResponse uploadPicture(com.entrytask.server.rpc.common.grpc.pictureRequest request) {
      return blockingUnaryCall(
          getChannel(), getUploadPictureMethod(), getCallOptions(), request);
    }

    /**
     */
    public com.entrytask.server.rpc.common.grpc.nicknameResponse updateNickname(com.entrytask.server.rpc.common.grpc.nicknameRequest request) {
      return blockingUnaryCall(
          getChannel(), getUpdateNicknameMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class ProfileServiceFutureStub extends io.grpc.stub.AbstractStub<ProfileServiceFutureStub> {
    private ProfileServiceFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private ProfileServiceFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected ProfileServiceFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new ProfileServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.entrytask.server.rpc.common.grpc.queryResponse> queryProfile(
        com.entrytask.server.rpc.common.grpc.queryRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getQueryProfileMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.entrytask.server.rpc.common.grpc.pictureResponse> uploadPicture(
        com.entrytask.server.rpc.common.grpc.pictureRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getUploadPictureMethod(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.entrytask.server.rpc.common.grpc.nicknameResponse> updateNickname(
        com.entrytask.server.rpc.common.grpc.nicknameRequest request) {
      return futureUnaryCall(
          getChannel().newCall(getUpdateNicknameMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_QUERY_PROFILE = 0;
  private static final int METHODID_UPLOAD_PICTURE = 1;
  private static final int METHODID_UPDATE_NICKNAME = 2;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final ProfileServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(ProfileServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_QUERY_PROFILE:
          serviceImpl.queryProfile((com.entrytask.server.rpc.common.grpc.queryRequest) request,
              (io.grpc.stub.StreamObserver<com.entrytask.server.rpc.common.grpc.queryResponse>) responseObserver);
          break;
        case METHODID_UPLOAD_PICTURE:
          serviceImpl.uploadPicture((com.entrytask.server.rpc.common.grpc.pictureRequest) request,
              (io.grpc.stub.StreamObserver<com.entrytask.server.rpc.common.grpc.pictureResponse>) responseObserver);
          break;
        case METHODID_UPDATE_NICKNAME:
          serviceImpl.updateNickname((com.entrytask.server.rpc.common.grpc.nicknameRequest) request,
              (io.grpc.stub.StreamObserver<com.entrytask.server.rpc.common.grpc.nicknameResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class ProfileServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    ProfileServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.entrytask.server.rpc.common.grpc.ProfileServiceOuterClass.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("ProfileService");
    }
  }

  private static final class ProfileServiceFileDescriptorSupplier
      extends ProfileServiceBaseDescriptorSupplier {
    ProfileServiceFileDescriptorSupplier() {}
  }

  private static final class ProfileServiceMethodDescriptorSupplier
      extends ProfileServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    ProfileServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (ProfileServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new ProfileServiceFileDescriptorSupplier())
              .addMethod(getQueryProfileMethod())
              .addMethod(getUploadPictureMethod())
              .addMethod(getUpdateNicknameMethod())
              .build();
        }
      }
    }
    return result;
  }
}
