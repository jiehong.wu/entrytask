package com.entrytask.server.http.filter;

import com.alibaba.fastjson.JSONObject;
import com.entrytask.server.http.common.grpc.*;
import com.entrytask.server.http.common.BaseContext;
import com.entrytask.server.http.common.GrpcClient;
import com.entrytask.server.http.common.Response;
import com.entrytask.server.http.config.WebAppConfig;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;



@Component
@WebFilter(filterName = "AuthFilter")
@Order(-1)
public class AuthFilter implements Filter {

    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        String reqUri = request.getRequestURI();

        for(String uri : WebAppConfig.whiteList){
            if(uri.equals(reqUri)){
                chain.doFilter(request, response);
                return;
            }
        }

        Cookie[] cookies = request.getCookies();
        if(cookies != null){
            for(Cookie cookie : cookies){
                if("token".equals(cookie.getName())){
                    String token = cookie.getValue();
                    authRequest authreq = authRequest.newBuilder().setToken(token).build();
                    authResponse authResponse = GrpcClient.getAuthStub().jwtAuth(authreq);
                    String username = authResponse.getMsg();
                    BaseContext.setThreadLocal(username);
                    chain.doFilter(request, response);
                    return;
                }
            }
        }
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json; charset=utf-8");
        response.getWriter().write(JSONObject.toJSONString(Response.failed("no token")));

    }
}
