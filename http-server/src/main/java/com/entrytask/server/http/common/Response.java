package com.entrytask.server.http.common;

import lombok.Data;


/**
 * Response 服务端响应
 * @author jiehong.wu
 * code:0 成功, 1 失败
 */
@Data
public class Response {
    private int code;
    private String msg;
    private Object data;

    public static Response success(String msg, Object data){
        Response resp = new Response();
        resp.setCode(0);
        resp.setMsg(msg);
        resp.setData(data);
        return resp;
    }

    public static Response failed(String msg){
        Response resp = new Response();
        resp.setCode(1);
        resp.setMsg(msg);
        return resp;
    }

    public static Response getNewResponse(int code, String msg, Object data){
        Response resp = new Response();
        resp.setCode(code);
        resp.setMsg(msg);
        resp.setData(data);
        return resp;
    }
}
