// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: ProfileService.proto

package com.entrytask.server.http.common.grpc;

public interface pictureRequestOrBuilder extends
    // @@protoc_insertion_point(interface_extends:com.entrytask.server.http.common.grpc.pictureRequest)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>string username = 1;</code>
   * @return The username.
   */
  java.lang.String getUsername();
  /**
   * <code>string username = 1;</code>
   * @return The bytes for username.
   */
  com.google.protobuf.ByteString
      getUsernameBytes();

  /**
   * <code>string picturePath = 2;</code>
   * @return The picturePath.
   */
  java.lang.String getPicturePath();
  /**
   * <code>string picturePath = 2;</code>
   * @return The bytes for picturePath.
   */
  com.google.protobuf.ByteString
      getPicturePathBytes();

  /**
   * <code>bytes file = 3;</code>
   * @return The file.
   */
  com.google.protobuf.ByteString getFile();

  /**
   * <code>string pictureName = 4;</code>
   * @return The pictureName.
   */
  java.lang.String getPictureName();
  /**
   * <code>string pictureName = 4;</code>
   * @return The bytes for pictureName.
   */
  com.google.protobuf.ByteString
      getPictureNameBytes();
}
