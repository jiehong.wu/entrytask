// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: UserService.proto

package com.entrytask.server.http.common.grpc;

public interface FileResponseOrBuilder extends
    // @@protoc_insertion_point(interface_extends:com.entrytask.server.http.common.grpc.FileResponse)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>string resp = 1;</code>
   * @return The resp.
   */
  java.lang.String getResp();
  /**
   * <code>string resp = 1;</code>
   * @return The bytes for resp.
   */
  com.google.protobuf.ByteString
      getRespBytes();
}
