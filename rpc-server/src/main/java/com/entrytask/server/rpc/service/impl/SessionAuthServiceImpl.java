package com.entrytask.server.rpc.service.impl;

import com.entrytask.server.rpc.service.SessionAuthService;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import javax.annotation.Resource;

@Service
public class SessionAuthServiceImpl implements SessionAuthService {
    @Resource(name = "redisTemplate0")
    private RedisTemplate<String, String> redisTemplate0;

    @Override
    public String authToken(String token){
        return redisTemplate0.opsForValue().get(token);
    }

    @Override
    public String generateToken(String username){
        String token = DigestUtils.md5DigestAsHex(username.getBytes());
        redisTemplate0.opsForValue().set(token, username);
        return token;
    }
}
