package com.entrytask.server.rpc.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.Map;


@Mapper
public interface UserDao {

    @Select("select password from user_system where username = #{username}")
    public String queryPasswordByUsername(String username);

    @Insert("insert into user_system(username, password) values(#{username}, #{password})")
    public void addUser(String username, String password);

    @Select("select id from user_system where username = #{username}")
    public Integer userIsExist(String username);

    @Update("update user_system set nickname=#{nickname} where username=#{username}")
    public void updateNickname(String username, String nickname);

    @Select("select username, nickname, profile from user_system where username=#{username}")
    public Map<String, Object> queryProfile(String username);

    @Update("update user_system set profile=#{path} where username=#{username}")
    public void updatePath(String username, String path);

}
