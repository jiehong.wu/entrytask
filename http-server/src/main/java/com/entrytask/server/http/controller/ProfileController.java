package com.entrytask.server.http.controller;

import com.entrytask.server.http.common.BaseContext;
import com.entrytask.server.http.common.Response;
import com.entrytask.server.http.service.ProfileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


@RestController
@RequestMapping("/api")
public class ProfileController {

    @Autowired
    private ProfileService profileService;

    @GetMapping("/user/profile")
    public Response getUserProfileHandler(){
        String username = BaseContext.getThreadLocal();
        return profileService.getUserProfile(username);
    }

    @PostMapping("/user/nickname")
    public Response updateNicknameHandler(@RequestParam("nickname") String nickname){
        String username = BaseContext.getThreadLocal();
        return profileService.updateNickname(username, nickname);
    }

    @PostMapping("/user/picture")
    public Response uploadProfileHandler(MultipartFile avatar){
        String username = BaseContext.getThreadLocal();
        return profileService.uploadPicture(username, avatar);

    }

}
