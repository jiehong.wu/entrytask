package com.entrytask.server.rpc.controller;

import com.entrytask.server.rpc.common.grpc.*;
import com.entrytask.server.rpc.model.UserModel;
import com.google.common.base.Stopwatch;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class UserController {


    @PostMapping("/user/login")
    String loginHandler(@RequestBody UserModel userDto){
        String userName = userDto.getUsername();
        String passWord = userDto.getPassword();
        System.out.println("name:"+userName);
        System.out.println("password:"+passWord);

        //grpc
        ManagedChannel channel = ManagedChannelBuilder.forAddress("127.0.0.1", 9998).usePlaintext().build();
        Stopwatch stopwatch = Stopwatch.createStarted();

        UserServiceGrpc.UserServiceBlockingStub stub = UserServiceGrpc.newBlockingStub(channel);
        loginRequest req = loginRequest.newBuilder().setUsername(userName).setPassword(passWord).build();
        loginResponse resp = stub.login(req);
        System.out.println("stop watch:"+ stopwatch.stop());

        System.out.println("get code:"+resp.getCode());
        System.out.println("get msg:"+ resp.getMsg());

        return "test";
    }

    //logout

}
