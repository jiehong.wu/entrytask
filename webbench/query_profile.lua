#!/usr/bin/lua5.1

request = function()
    local method = "GET"
    local path = "/api/user/profile"
    local headers = {}
    local token = "token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJwYXNzd29yZCI6IjEyMyIsIm5hbWUiOiJ4aWFvbWluZyIsImV4cCI6MTY2MTkzNDY0MywiaWF0IjoxNjYxOTI4NjQzfQ.3GapPAXt68EBweknOUn7h5QpMnS3EFXyz0d8KDyPGuw"
    headers["Cookie"] = token
    return wrk.format(method, path, headers, body)
end