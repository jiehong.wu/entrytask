package com.entrytask.server.rpc.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class JwtAuthServiceTest {
    @Autowired
    private JwtAuthService jwtAuthService;

    @Test
    public void generateTokenTest(){
        String token = jwtAuthService.generateToken("xiaoming", "123");
        Assert.assertNotNull(token);
    }

    @Test
    public void generateTokenData(){
        for(int i= 10000; i < 12000; i++){
            String username = "user_test_"+i;
            String token = jwtAuthService.generateToken(username, "123");
            System.out.println("\"token="+token+"\""+",");
        }
    }
}