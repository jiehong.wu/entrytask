package com.entrytask.server.http.config;

import com.entrytask.server.http.common.GrpcClient;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import java.util.ArrayList;

@Configuration
public class WebAppConfig {
    private final String LOGIN_PATH = "/api/user/login";
    private final String REGISTER_PATH = "/api/user/register";

    public static ArrayList<String> whiteList;

    @PostConstruct
    public void grpcClientInit(){
        GrpcClient.grpcClientInit();
    }

    @PostConstruct
    public void whileListInit(){
        whiteList = new ArrayList<>();
        whiteList.add(LOGIN_PATH);
        whiteList.add(REGISTER_PATH);
    }
}
