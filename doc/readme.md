# 一、系统需求

- 实现一个用户管理系统，用户可以登录、拉取和编辑他们的profiles。

- 用户可以通过在Web页面输入username和password登录，backend系统负责校验用户身份。成功登录 后，页面需要展示用户的相关信息;否则页面展示相关错误。

- 成功登录后，用户可以编辑以下内容:

   1. 上传profile picture

  2. 修改nickname(需要支持Unicode字符集，utf-8编码) 




# 二、系统设计
## 2.1 系统架构图

![user_system_架构](./user_system_架构.jpg)



## 2.2 系统业务架构设计

![entrytask业务架构图](./业务架构图.png)





## 2.3 目录结构

```text
http-server
├── common -- 工具类及通用代码
├── config -- 初始化相关资源
├── controller -- 路由接口
├── model -- 数据模型类
├── filter -- 过滤器
├── service -- 相关服务代码
├── pb -- grpc协议相关代码
└── dto -- 对接前端的数据模型

rpc-server
├── common -- 工具类及通用代码
├── model -- 数据模型类
├── dao -- 连接数据库相关代码
├── pb -- grpc协议相关代码
└── service -- grpc相关服务实现
```







## 2.4 核心模块设计

### 2.4.1 登陆注册模块

- 登陆，注册功能的实现比较类似，关键在于redis与mysql数据的同步

- 权限认证通过JWT的方式实现

- 登陆流程

  ![登陆流程.drawio](/Users/jiehong.wu/java_learning/Entry-Task/doc/登陆流程.drawio.png)



### 2.4.2 信息查询模块

- 实现思路：
  1. 判断cookie中是否存在token，若不存在则返回认证失败
  2. 调用RPC服务解析token并获取用户名和ID
  3. 调用RPC服务查询用户信息
  4. 更新缓存并返回用户信息

- 信息查询流程

![信息查询流程](./信息查询流程.png)



### 2.4.3 文件服务模块

- 实现思路：

  1. 使用nginx作为文件服务器，保存用户的头像图片
  2. mysql,redis中保存用户头像图片的地址
  3. 在查询请求到来时查询数据库并返回图片地址给前端
  4. 前端页面通过得到的图片地址访问nginx

- 文件传输流程

  ![图片传输](./图片传输.png)



# 三、接口设计

- 系统对外提供五个接口
  - 用户登录接口
  - 用户注册接口
  - 用户更新昵称接口
  - 用户上传头像接口
  - 用户信息查询接口



## 3.1 用户登录接口

- /api/user/login

- Method:POST

- 请求参数

  - |  字段名  |  类型  | 是否必填 |  含义  |
    | :------: | :----: | :------: | :----: |
    | username | string |    是    | 用户名 |
    | Password | string |    是    |  密码  |

- 响应参数

  - | 通用字段名 |  类型  | 是否必填 |       含义       |
    | :--------: | :----: | :------: | :--------------: |
    |    code    |  int   |    是    |     响应状态     |
    |    msg     | string |    是    | 响应状态描述信息 |
    |    data    | object |    否    |   具体数据对象   |

- 返回示例

  - 登陆成功

    ```json
    {
    	"code":0,
      "msg":"login successful",
      "data":null
    }
    ```

  - 登陆失败

    ```json
    {
    	"code":1,
      "msg":"username or password wrong"
      "data":null
    }
    ```

    



## 3.2 用户注册接口

- api/user/register

- Method:POST

- 请求参数

  - |  字段名  |  类型  | 是否必填 |  含义  |
    | :------: | :----: | :------: | :----: |
    | username | string |    是    | 用户名 |
    | Password | string |    是    |  密码  |

- 响应参数

  - | 通用字段名 |  类型  | 是否必填 |       含义       |
    | :--------: | :----: | :------: | :--------------: |
    |    code    |  int   |    是    |     响应状态     |
    |    msg     | string |    是    | 响应状态描述信息 |
    |    data    | object |    否    |   具体数据对象   |

- 返回示例

  - 登陆成功

    ```json
    {
    	"code":0,
      "msg":"register successful",
      "data":null
    }
    ```

  - 登陆失败

    ```json
    {
    	"code":1,
      "msg":"username already exist"
      "data":null
    }
    ```

    





## 3.3 用户更新头像接口

- api/user/picture

- Method:POST

- 该接口从cookie中获取username

- 请求参数

  - | 字段名 | 类型 | 是否必填 |   含义   |
    | :----: | :--: | :------: | :------: |
    | avatar | File |    是    | 图片信息 |

- 响应参数

  - | 通用字段名 |  类型  | 是否必填 |       含义       |
    | :--------: | :----: | :------: | :--------------: |
    |    code    |  int   |    是    |     响应状态     |
    |    msg     | string |    是    | 响应状态描述信息 |
    |    data    | object |    否    |   具体数据对象   |

- 返回示例

  - 登陆成功

    ```json
    {
        "code": 0,
        "msg": "上传成功",
        "data": null
    }
    ```

  - 登陆失败

    ```json
    {
        "code": 1,
        "msg": "上传失败",
        "data": null
    }
    ```

    





## 3.4 用户更新昵称接口

- api/user/nickname

- Method:POST

- 该接口从cookie中读取username

- 请求参数

  - |  字段名  |  类型  | 是否必填 | 含义 |
    | :------: | :----: | :------: | :--: |
    | Nickname | string |    是    | 昵称 |

- 响应参数

  - | 通用字段名 |  类型  | 是否必填 |       含义       |
    | :--------: | :----: | :------: | :--------------: |
    |    code    |  int   |    是    |     响应状态     |
    |    msg     | string |    是    | 响应状态描述信息 |
    |    data    | object |    否    |   具体数据对象   |

- 返回示例

  - 更新成功

    ```json
    {
        "code": 0,
        "msg": "update nickname successful",
        "data": null
    }
    ```

  - 更新失败

    ```json
    {
        "code": 1,
        "msg": "failed to update nickname fail",
        "data": null
    }
    ```

    





## 3.5 用户信息查询接口

- api/user/profile

- Method:GET

- 该接口从cookie中读取username

- 响应参数

  - | 通用字段名 |  类型  | 是否必填 |       含义       |
    | :--------: | :----: | :------: | :--------------: |
    |    code    |  int   |    是    |     响应状态     |
    |    msg     | string |    是    | 响应状态描述信息 |
    |    data    | object |    否    |   具体数据对象   |

- 返回示例

  - 查询成功

    ```json
    {
        "code": 0,
        "msg": "query success",
        "data": {
            "id": null,
            "username": "xiaoming",
            "password": null,
            "nickname": "小米",
            "profile": "304ycu5prgo.jpeg"
        }
    }
    ```

  - 查询失败

    ```json
    {
        "code": 1,
        "msg": "query failed",
        "data": null
    }
    ```

    



# 四、项目部署

## 4.1 运行环境

- 操作系统：macOS
- 芯片：Apple M1 Pro
- 核心数：10（8性能，2效能）
- 内存：16GB

## 4.2 数据库环境

- MySQL数据库

  - 准备数据库表

    ```
    +----------+--------------+------+-----+---------+----------------+
    | Field    | Type         | Null | Key | Default | Extra          |
    +----------+--------------+------+-----+---------+----------------+
    | id       | bigint       | NO   | PRI | NULL    | auto_increment |
    | username | varchar(255) | NO   | UNI | NULL    |                |
    | password | varchar(255) | NO   |     | NULL    |                |
    | nickname | varchar(255) | YES  |     | NULL    |                |
    | profile  | varchar(255) | YES  |     | NULL    |                |
    +----------+--------------+------+-----+---------+----------------+
    ```

  - 通过以下python脚本生成1000万数据

    ```
    import time
    import pymysql.cursors
    
    conn = pymysql.connect(
       host='127.0.0.1',
       user='root',
       password='12345678',
       database='entry_task',
       charset='utf8mb4',
       cursorclass=pymysql.cursors.DictCursor
    )
    
    print("hello")
    
    cursor = conn.cursor()
    
    sql = """INSERT INTO user_system (username,password,nickname) VALUES("user_test_{}", "123", "nickname_{}");"""
    
    print(sql)
    
    for i in range(1, 10000002):
       try:
          tmp_sql = sql.format(i,i)
          cursor.execute(tmp_sql)
          if not i % 10000:
             conn.commit()
             print(i)
          
    
       except Exception as e:
          print(e)
    
    conn.commit()
    conn.close()
    
    ```

- redis数据库

  - 通过./redis-server启动数据库服务器即可



## 4.3 项目运行

1. 通过idea直接运行http-server以及rpc-server

2. 生成jar包后通过命令行启动

   ```
   java -jar http-server.jar
   java -jar rpc-server.jar
   ```

   



# 五、性能测试

- 使用wrk进行性能测试

## 5.1登陆接口

- 200固定用户(清空redis后执行)

  ```
  jiehong.wu@H22GNWY0WD webbench % wrk -c200 -t8 -d30s -s login_request.lua  http://127.0.0.1:10020/api/user/login 
  Running 30s test @ http://127.0.0.1:10020/api/user/login
    8 threads and 200 connections
    Thread Stats   Avg      Stdev     Max   +/- Stdev
      Latency    14.51ms   10.63ms 292.34ms   89.02%
      Req/Sec     1.82k   211.22     2.20k    79.77%
    434543 requests in 30.05s, 275.66MB read
    Socket errors: connect 0, read 179, write 0, timeout 0
  Requests/sec:  14462.57
  Transfer/sec:      9.17MB
  ```

  

- 2000固定用户(清空redis后执行)

```
jiehong.wu@H22GNWY0WD webbench % wrk -c2000 -t8 -d60s -s login_request.lua  http://127.0.0.1:10020/api/user/login 
Running 1m test @ http://127.0.0.1:10020/api/user/login
  8 threads and 2000 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    17.87ms    8.82ms 119.58ms   78.04%
    Req/Sec     1.72k   557.70     6.63k    63.97%
  822609 requests in 1.00m, 521.84MB read
  Socket errors: connect 1755, read 118, write 56, timeout 0
Requests/sec:  13687.37
Transfer/sec:      8.68MB
```



- 200随机用户(清空redis后执行)

```
jiehong.wu@H22GNWY0WD webbench % wrk -c200 -t8 -d60s -s random_user.lua http://127.0.0.1:10020 
Running 30s test @ http://127.0.0.1:10020
  8 threads and 200 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    16.19ms    8.75ms 118.41ms   80.40%
    Req/Sec     1.60k   227.59     2.01k    71.69%
  383025 requests in 30.09s, 246.27MB read
  Socket errors: connect 0, read 182, write 1, timeout 0
Requests/sec:  12728.91
Transfer/sec:      8.18MB
```



- 2000随机用户(清空redis后执行)

```
jiehong.wu@H22GNWY0WD webbench % wrk -c2000 -t8 -d60s -s random_user.lua http://127.0.0.1:10020 
Running 30s test @ http://127.0.0.1:10020
  8 threads and 2000 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   166.89ms   80.88ms 951.76ms   93.20%
    Req/Sec     1.57k   316.86     2.03k    86.95%
  362747 requests in 30.10s, 233.20MB read
  Socket errors: connect 0, read 19359, write 0, timeout 0
Requests/sec:  12049.57
Transfer/sec:      7.75MB
```



- **结果分析**

|    测试项    | 200固定用户 | 2000固定用户 | 200随机用户 | 2000随机用户 |
| :----------: | :---------: | :----------: | :---------: | :----------: |
| 平均每秒流量 |   9.17MB    |    8.68MB    |   8.18MB    |    7.75MB    |
|     QPS      |  14462.57   |   13687.37   |  12728.91   |   12049.57   |
|   目标QPS    |    3000     |     1500     |    1000     |     800      |

- CPU占用率情况

  Http-server平均在400%

  RPC-server平均在300%

  ![image-20220901114130645](./cpu.png)





## 5.2查询接口

- 200固定用户(清空redis后执行)

```
jiehong.wu@H22GNWY0WD webbench % wrk -c200 -t8 -d30s -s query_profile.lua http://127.0.0.1:10020  
Running 30s test @ http://127.0.0.1:10020
  8 threads and 200 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    24.21ms   21.59ms 408.50ms   98.35%
    Req/Sec     1.12k   146.50     1.68k    74.96%
  265781 requests in 30.09s, 128.56MB read
  Socket errors: connect 0, read 130, write 1, timeout 0
Requests/sec:   8832.03
Transfer/sec:      4.27MB
```



- 2000固定用户(清空redis后执行)

```
jiehong.wu@H22GNWY0WD webbench % wrk -c2000 -t8 -d30s -s query_profile.lua http://127.0.0.1:10020 
Running 30s test @ http://127.0.0.1:10020
  8 threads and 2000 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   249.76ms   65.46ms   1.38s    96.13%
    Req/Sec     1.00k   272.18     1.69k    77.03%
  230650 requests in 30.06s, 111.56MB read
  Socket errors: connect 0, read 23856, write 329, timeout 0
Requests/sec:   7672.26
Transfer/sec:      3.71MB
```



- 200随机用户(清空redis后执行)

```
jiehong.wu@H22GNWY0WD webbench % wrk -c200 -t8 -d30s -s random_profile.lua http://127.0.0.1:10020 
Running 30s test @ http://127.0.0.1:10020
  8 threads and 200 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency    24.72ms    8.03ms  83.25ms   82.63%
    Req/Sec     1.02k   152.02     1.37k    67.82%
  244000 requests in 30.09s, 118.72MB read
  Socket errors: connect 0, read 84, write 0, timeout 0
Requests/sec:   8107.79
Transfer/sec:      3.94MB
```



- 2000随机用户(清空redis后执行)

```
jiehong.wu@H22GNWY0WD webbench % wrk -c2000 -t8 -d30s -s random_profile.lua http://127.0.0.1:10020 
Running 30s test @ http://127.0.0.1:10020
  8 threads and 2000 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   266.29ms   72.80ms   1.00s    96.41%
    Req/Sec     0.96k   210.00     1.51k    76.43%
  221524 requests in 30.10s, 107.78MB read
  Socket errors: connect 0, read 18973, write 120, timeout 0
Requests/sec:   7358.39
Transfer/sec:      3.58MB
```



- **结果分析**
  - 相比登陆接口QPS有所下降主要是因为查询接口中调用了2次GRPC服务


|    测试项    | 200固定用户 | 2000固定用户 | 200随机用户 | 2000随机用户 |
| :----------: | :---------: | :----------: | :---------: | :----------: |
| 平均每秒流量 |   4.27MB    |    3.71MB    |   3.94MB    |    3.58MB    |
|     QPS      |   8832.03   |   7672.26    |   8107.79   |   7358.39    |
|   目标QPS    |    3000     |     1500     |    1000     |     800      |



