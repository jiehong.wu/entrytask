package com.entrytask.server.http.common;

public class BaseContext {
    private static ThreadLocal<String> threadLocal = new ThreadLocal<>();

    public static void setThreadLocal(String token){
        threadLocal.set(token);
    }

    public static String getThreadLocal(){
        return threadLocal.get();
    }

    public static void removeThreadLocal(){
        threadLocal.remove();
    }
}
