package com.entrytask.server.rpc.service.impl;

import com.auth0.jwt.interfaces.Claim;
import com.entrytask.server.rpc.common.grpc.*;
import com.entrytask.server.rpc.common.JwtUtil;
import com.entrytask.server.rpc.service.SessionAuthService;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

/**
 * @author jiehong.wu
 */
@GrpcService
public class AuthServiceImpl extends AuthServiceGrpc.AuthServiceImplBase {
    @Autowired
    SessionAuthService sessionAuthService;

    /**
     * authMethod: 1:session认证，2：jwt token认证
     */
    private final int authMethod = 2;


    @Override
    public void jwtAuth(authRequest authRequest, StreamObserver<authResponse> responseObServer) {
        String token = authRequest.getToken();

        authResponse resp;

        if(authMethod == 1){
            String username = sessionAuthService.authToken(token);
            if (username != null){
                resp = authResponse.newBuilder().setCode(1).setMsg(username).build();
            }else{
                resp = authResponse.newBuilder().setCode(2).setMsg("auth failed").build();
            }
            responseObServer.onNext(resp);
            responseObServer.onCompleted();
            return;
        }

        Map<String, Claim> claimData = JwtUtil.verifyToken(token);

        if (claimData != null){
            resp = authResponse.newBuilder().setCode(1).setMsg(claimData.get("name").asString()).build();
        }else{
            resp = authResponse.newBuilder().setCode(2).setMsg("auth failed").build();
        }
        responseObServer.onNext(resp);
        responseObServer.onCompleted();
    }
}
