package com.entrytask.server.rpc.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class SessionAuthServiceTest {
    @Autowired
    private SessionAuthService sessionAuthService;

    @Test
    public void sessionTokenTest(){
        String token = sessionAuthService.generateToken("xiaoming");
        String username = sessionAuthService.authToken(token);
        Assert.assertEquals(username, "xiaoming");
    }
}