package com.entrytask.server.rpc.service.impl;

import com.entrytask.server.rpc.common.JwtUtil;
import com.entrytask.server.rpc.service.JwtAuthService;
import org.springframework.stereotype.Service;

@Service
public class JwtAuthServiceImpl implements JwtAuthService {
    @Override
    public String generateToken(String username, String password){
        return JwtUtil.createToken(username, password);
    }
}
