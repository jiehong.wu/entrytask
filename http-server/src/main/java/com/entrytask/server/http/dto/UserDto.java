package com.entrytask.server.http.dto;

import com.entrytask.server.http.model.UserModel;


import lombok.Data;


@Data
public class UserDto extends UserModel {
}
