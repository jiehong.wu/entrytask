package com.entrytask.server.rpc.service.impl;

import com.entrytask.server.rpc.common.grpc.*;
import com.entrytask.server.rpc.dao.UserDao;
import com.entrytask.server.rpc.model.ProfileModel;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

@GrpcService
public class ProfileServiceImpl extends ProfileServiceGrpc.ProfileServiceImplBase {

    @Autowired
    private UserDao userDao;

    @Resource
    private RedisTemplate<String, ProfileModel> redisTemplate;

    @Override
    public void queryProfile(queryRequest req, StreamObserver<queryResponse> responseObServer) {
        String username = req.getUsername();

        ProfileModel queryObject;

        Map<String,Object> data;

        queryResponse queryResp;

        //redis
        queryObject = redisTemplate.opsForValue().get(username);
        if (queryObject != null && queryObject.getNickname() != null && queryObject.getProfile() != null){
            queryResp = queryResponse.newBuilder().setUsername(username)
                    .setNickname(queryObject.getNickname())
                    .setPicture(queryObject.getProfile())
                    .build();
            responseObServer.onNext(queryResp);
            responseObServer.onCompleted();
            return;
        }

        data = userDao.queryProfile(username);

        if(data == null){
            queryResp = queryResponse.newBuilder().setUsername("").build();
            responseObServer.onNext(queryResp);
            responseObServer.onCompleted();
            return;
        }

        if(queryObject == null){
            queryObject = new ProfileModel();
        }

        Object objectNickname = data.get("nickname");
        Object objectProfile = data.get("profile");
        String nickname = objectIsExist(objectNickname);
        String profile = objectIsExist(objectProfile);


        queryObject.setNickname(nickname);
        queryObject.setProfile(profile);
        redisTemplate.opsForValue().set(username, queryObject);

        queryResp = queryResponse.newBuilder().setUsername(username)
                .setNickname(nickname)
                .setPicture(profile)
                .build();
        responseObServer.onNext(queryResp);
        responseObServer.onCompleted();
    }

    private String objectIsExist(Object object){
        if (object == null){
            return "";
        }else{
            return object.toString();
        }
    }

    @Override
    public void uploadPicture(pictureRequest request, StreamObserver<pictureResponse> responseObServer) {
        byte[] bytes = request.getFile().toByteArray();
        String path = request.getPicturePath();
        ProfileModel queryObject;

        File f = new File(path);
        pictureResponse response;
        if (f.exists()) {
            f.delete();
        }
        try (OutputStream os = new FileOutputStream(f)) {
            os.write(bytes);
            response = pictureResponse.newBuilder().setCode(0).setMsg("上传成功").build();
            System.out.println("filename:"+request.getPictureName());
            userDao.updatePath(request.getUsername() ,request.getPictureName());


            //同步redis
            queryObject = redisTemplate.opsForValue().get(request.getUsername());
            if(queryObject == null){
                queryObject = new ProfileModel();
            }
            queryObject.setProfile(request.getPictureName());
            redisTemplate.opsForValue().set(request.getUsername(), queryObject);

        } catch (IOException e) {
            response = pictureResponse.newBuilder().setCode(1).setMsg(String.format("上传失败:%s", e.getMessage())).build();
            e.printStackTrace();
        }
        // 返回数据，完成此次请求
        responseObServer.onNext(response);
        responseObServer.onCompleted();
    }
    
    @Override
    public void updateNickname(nicknameRequest request, StreamObserver<nicknameResponse> responseObServer) {
        String username = request.getUsername();
        String nickname = request.getNickname();
        ProfileModel queryObject;

        userDao.updateNickname(username, nickname);

        //同步redis
        queryObject = redisTemplate.opsForValue().get(username);
        if(queryObject == null){
            queryObject = new ProfileModel();
        }
        queryObject.setNickname(nickname);
        redisTemplate.opsForValue().set(username, queryObject);

        nicknameResponse resp;
        resp = nicknameResponse.newBuilder().setCode(1).setMsg("update nickname success").build();
        responseObServer.onNext(resp);
        responseObServer.onCompleted();
    }

}
