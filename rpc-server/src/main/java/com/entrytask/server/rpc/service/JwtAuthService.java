package com.entrytask.server.rpc.service;


public interface JwtAuthService {
    public String generateToken(String username, String password);
}
