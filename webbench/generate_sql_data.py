import time
import pymysql.cursors

conn = pymysql.connect(
   host='127.0.0.1',
   user='root',
   password='12345678',
   database='entry_task',
   charset='utf8mb4',
   cursorclass=pymysql.cursors.DictCursor
)

print("hello")

cursor = conn.cursor()

sql = """INSERT INTO user_system (username,password,nickname) VALUES("user_test_{}", "123", "nickname_{}");"""

print(sql)

# 10000001
for i in range(1, 10000002):
   try:
      tmp_sql = sql.format(i,i)
      cursor.execute(tmp_sql)
      if not i % 10000:
         conn.commit()
         print(i)
      

   except Exception as e:
      print(e)

# Closing the connection
conn.commit()
conn.close()
