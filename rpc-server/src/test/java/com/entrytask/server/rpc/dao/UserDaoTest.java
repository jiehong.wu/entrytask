package com.entrytask.server.rpc.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.Map;


@SpringBootTest
@RunWith(SpringRunner.class)
public class UserDaoTest {
    @Autowired
    private UserDao userDao;

    @Test
    public void userIsExistTest(){
        Integer id = userDao.userIsExist("admi12n");
        System.out.println("id:"+id);
        if(id == null){
            System.out.println("id is null");
        }
    }

    @Test
    public void queryPasswordByUsernameTest(){
        try {
            String password = userDao.queryPasswordByUsername("admin1221");
            System.out.println("password:"+password);
            if(password.isEmpty()){
                System.out.println("password is null");
            }
        }catch (Exception e){
            System.out.println("execpiton:"+e.getMessage());
        }
    }

    @Test
    public void updateNicknameTest(){
        userDao.updateNickname("xiaoming", "ttt");
    }

    @Test
    public void queryProfileTest(){
        Map<String, Object> profile = userDao.queryProfile("123");
        if(profile == null){
            System.out.println("null");
            return;
        }

        for(Map.Entry<String, Object> entry : profile.entrySet()){
            System.out.println(entry.getKey()+entry.getValue());
        }

    }

    @Test
    public void updatePahtTest(){
        userDao.updatePath("xiaoming2", "ttt2");
    }

}
