package com.entrytask.server.http.service;

import com.entrytask.server.http.common.Response;
import org.springframework.web.multipart.MultipartFile;

public interface ProfileService {
    public Response getUserProfile(String username);

    public Response updateNickname(String username, String nickname);

    public Response uploadPicture(String username, MultipartFile avatar);
}
