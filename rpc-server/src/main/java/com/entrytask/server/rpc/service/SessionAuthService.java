package com.entrytask.server.rpc.service;

public interface SessionAuthService {
    public String authToken(String token);

    public String generateToken(String username);
}
