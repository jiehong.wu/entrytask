package com.entrytask.server.rpc.service.impl;

import com.entrytask.server.rpc.common.grpc.*;
import com.entrytask.server.rpc.dao.UserDao;
import com.entrytask.server.rpc.model.ProfileModel;
import com.entrytask.server.rpc.service.JwtAuthService;
import com.entrytask.server.rpc.service.SessionAuthService;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import javax.annotation.Resource;



@GrpcService
@Slf4j
public class UserServiceImpl extends UserServiceGrpc.UserServiceImplBase {
    @Autowired
    private UserDao userDao;

    @Autowired
    private JwtAuthService jwtAuthService;

    @Autowired
    private SessionAuthService sessionAuthService;

    //1:session认证， 2:jwt认证
    private int authMethod = 2;

    @Resource
    private RedisTemplate<String, ProfileModel> redisTemplate;

    @Resource(name = "redisTemplate0")
    private RedisTemplate<String, String> redisTemplate0;
    @Override
    public void login(loginRequest request, StreamObserver<loginResponse> responseObServer) {
        String userName = request.getUsername();
        String password = request.getPassword();

        loginResponse resp;
        ProfileModel queryObject;

        try{
            queryObject = redisTemplate.opsForValue().get(userName);
            if (queryObject != null && queryObject.getPassword() != null){

                if(password.equals(queryObject.getPassword())){
                    if(authMethod == 1){
                        String sessionToken = sessionAuthService.generateToken(userName);
                        resp = loginResponse.newBuilder().setCode(1).setMsg(sessionToken).build();
                    }else{
                        String token = jwtAuthService.generateToken(userName, password);
                        resp = loginResponse.newBuilder().setCode(1).setMsg(token).build();
                    }
                }else{
                    resp = loginResponse.newBuilder().setCode(2).setMsg("username or password error").build();
                }
            }else {
                String queryPassword = userDao.queryPasswordByUsername(userName);
                if (password.equals(queryPassword)) {
                    if(queryObject == null){
                        queryObject = new ProfileModel();
                    }
                    queryObject.setPassword(password);
                    redisTemplate.opsForValue().set(userName, queryObject);

                    String token = jwtAuthService.generateToken(userName, password);
                    resp = loginResponse.newBuilder().setCode(1).setMsg(token).build();
                }else{
                    resp = loginResponse.newBuilder().setCode(2).setMsg("username or password error").build();
                }
            }
            responseObServer.onNext(resp);
            responseObServer.onCompleted();

        }catch (Exception e){
            System.out.println(e.getMessage());
            resp = loginResponse.newBuilder().setCode(2).setMsg("login failed").build();
            responseObServer.onNext(resp);
            responseObServer.onCompleted();
        }
    }

    @Override
    public void register(loginRequest request, StreamObserver<loginResponse> responseObServer) {
        String userName = request.getUsername();
        String password = request.getPassword();
        loginResponse resp;

        try{
            Integer id = userDao.userIsExist(userName);
            if(id != null){
                resp = loginResponse.newBuilder().setCode(2).setMsg("username is already exist").build();
                responseObServer.onNext(resp);
                responseObServer.onCompleted();
                return;
            }
            userDao.addUser(userName, password);
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("insert user Exception:"+e.getMessage());
            resp = loginResponse.newBuilder().setCode(2).setMsg("register failed").build();
            responseObServer.onNext(resp);
            responseObServer.onCompleted();
            return;
        }

        resp = loginResponse.newBuilder().setCode(1).setMsg("register success").build();
        responseObServer.onNext(resp);
        responseObServer.onCompleted();
    }

}
