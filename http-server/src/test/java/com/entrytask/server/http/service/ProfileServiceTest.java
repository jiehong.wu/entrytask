package com.entrytask.server.http.service;

import com.entrytask.server.http.common.Response;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@RunWith(SpringRunner.class)
public class ProfileServiceTest {
    @Autowired
    private ProfileService profileService;

    @Test
    public void getUserProfileTest(){
        Response resp = profileService.getUserProfile("xiaoming");
        Assert.assertNotNull(resp);
    }

    @Test
    public void updateNicknameTest(){
        Response resp = profileService.updateNickname("admin9", "蜡笔小新");
        Assert.assertNotNull(resp);
    }

}