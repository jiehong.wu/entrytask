package com.entrytask.server.http.service.impl;

import com.entrytask.server.http.common.grpc.*;
import com.entrytask.server.http.common.GrpcClient;
import com.entrytask.server.http.common.Response;
import com.entrytask.server.http.dto.UserDto;
import com.entrytask.server.http.service.ProfileService;
import com.google.common.base.Stopwatch;
import com.google.protobuf.ByteString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @author jiehong.wu
 */
@Service
@Slf4j
public class ProfileServiceImpl implements ProfileService {
    @Override
    public Response getUserProfile(String username){
        if (username == null){
            log.warn("username is null");
            return Response.failed("username is null");
        }
        queryRequest req = queryRequest.newBuilder().setUsername(username).build();
        queryResponse resp = GrpcClient.getProfileStub().queryProfile(req);
        UserDto userDto = new UserDto();
        userDto.setUsername(resp.getUsername());
        userDto.setNickname(resp.getNickname());
        userDto.setProfile(resp.getPicture());
        log.info("user {} query profile", username);
        return Response.success("query success",userDto);
    }

    @Override
    public Response updateNickname(String username, String nickname){

        nicknameRequest req = nicknameRequest.newBuilder().setUsername(username).setNickname(nickname).build();
        Stopwatch stopwatch = Stopwatch.createStarted();
        nicknameResponse resp = GrpcClient.getProfileStub().updateNickname(req);
        log.info("update nickname rpc using :{}", stopwatch);

        if(resp.getCode() == 1){
            return Response.success(resp.getMsg(), null);
        }else{
            return Response.failed("update nickname failed");
        }
    }

    @Override
    public Response uploadPicture(String username, MultipartFile avatar){
        if(avatar == null){
            System.out.println("no file selected");
            return Response.failed("上传失败，未选择文件");
        }
        String fileName = avatar.getOriginalFilename();
        String filePath = "/opt/homebrew/Cellar/nginx/1.23.1/html/"+fileName;
        pictureResponse resp;

        try {
            pictureRequest req = pictureRequest.newBuilder()
                    .setUsername(username)
                    .setPicturePath(filePath)
                    .setFile(ByteString.copyFrom(avatar.getBytes()))
                    .setPictureName(fileName)
                    .build();

            Stopwatch stopwatch = Stopwatch.createStarted();
            resp = GrpcClient.getProfileStub().uploadPicture(req);
            log.info("upload file using: {}", stopwatch);
            if(resp.getCode() == 0){
                log.info("user {} upload file", username);
                return Response.success(resp.getMsg(),null);
            }else{
                log.warn("upload file failed, {}",resp.getMsg());
                return Response.failed(resp.getMsg());
            }
        } catch (IOException e) {
            log.error("upload file error,{}",e.getMessage());
            return Response.failed("upload file error");
        }
    }
}
