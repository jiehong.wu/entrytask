package com.entrytask.server.http.controller;

import com.entrytask.server.http.common.GrpcClient;
import com.entrytask.server.http.common.Response;
import com.entrytask.server.http.common.grpc.*;
import com.entrytask.server.http.dto.UserDto;
import com.entrytask.server.http.service.UserService;
import com.google.common.base.Stopwatch;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletResponse;



@RestController
@RequestMapping("/api")
@Slf4j
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/user/login")
    Response loginHandler(@RequestBody UserDto userDto, HttpServletResponse httpServletResponse){
        String userName = userDto.getUsername();
        String passWord = userDto.getPassword();

        return userService.login(userName, passWord, httpServletResponse);
    }

    @PostMapping("/user/register")
    Response registerHandler(@RequestBody UserDto userDto){
        String userName = userDto.getUsername();
        String passWord = userDto.getPassword();

        loginRequest req = loginRequest.newBuilder().setUsername(userName).setPassword(passWord).build();

        Stopwatch stopwatch = Stopwatch.createStarted();
        loginResponse resp = GrpcClient.getUserStub().register(req);
        log.info("register stop watch:{}", stopwatch.stop());

        if(1 == resp.getCode()){
            return Response.success(resp.getMsg(), null);
        }else{
            return Response.failed(resp.getMsg());
        }
    }


}
