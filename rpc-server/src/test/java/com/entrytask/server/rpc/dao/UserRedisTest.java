package com.entrytask.server.rpc.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;


@SpringBootTest
@RunWith(SpringRunner.class)
public class UserRedisTest {

    @Autowired
    StringRedisTemplate stringRedisTemplate;


    @Test
    public void redisTest(){
        stringRedisTemplate.opsForValue().set("key1", "valueKey1");

        String str = stringRedisTemplate.opsForValue().get("key12");
        if(str == null){
            System.out.println("is null");
        }
        System.out.println("key:"+str);

        stringRedisTemplate.delete("key1");
    }
}
