package com.entrytask.server.http.common;

import com.entrytask.server.http.common.grpc.*;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;


public class GrpcClient {
    private static UserServiceGrpc.UserServiceBlockingStub userStub;

    private static ProfileServiceGrpc.ProfileServiceBlockingStub  profileStub;

    private static AuthServiceGrpc.AuthServiceBlockingStub authStub;

    public static void grpcClientInit(){
        ManagedChannel channel = ManagedChannelBuilder.forAddress("127.0.0.1", 9998).usePlaintext().build();
        userStub = UserServiceGrpc.newBlockingStub(channel);
        profileStub = ProfileServiceGrpc.newBlockingStub(channel);
        authStub = AuthServiceGrpc.newBlockingStub(channel);
    }

    public static UserServiceGrpc.UserServiceBlockingStub getUserStub(){
        return userStub;
    }

    public static ProfileServiceGrpc.ProfileServiceBlockingStub getProfileStub(){
        return profileStub;
    }

    public static AuthServiceGrpc.AuthServiceBlockingStub getAuthStub(){
        return authStub;
    }


}
