package com.entrytask.server.http.service.impl;

import com.entrytask.server.http.common.GrpcClient;
import com.entrytask.server.http.common.Response;
import com.entrytask.server.http.common.grpc.*;
import com.entrytask.server.http.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

/**
 * @author jiehong.wu
 */
@Slf4j
@Service
public class UserServiceImpl implements UserService {
    @Override
    public Response login(String username, String password, HttpServletResponse httpServletResponse){

        if(username == null || password == null){
            log.warn("username or password is null");
            return Response.failed("username or password wrong");
        }

        //grpc
        loginRequest req = loginRequest.newBuilder().setUsername(username).setPassword(password).build();
        loginResponse resp = GrpcClient.getUserStub().login(req);

        String token = resp.getMsg();

        if(1 == resp.getCode()){
            Cookie cookie = new Cookie("token", token);
            cookie.setMaxAge(60*60);
            httpServletResponse.addCookie(cookie);
            log.info("user {} login", username);
            return Response.success("login success", null);
        }else{
            log.warn("login happen error:{}", resp.getMsg());
            return Response.failed(resp.getMsg());
        }
    }

    @Override
    public Response register(String username, String password){
        if(username == null || password == null){
            log.warn("username or password is null");
            return Response.failed("username or password wrong");
        }

        //grpc
        loginRequest req = loginRequest.newBuilder().setUsername(username).setPassword(password).build();
        loginResponse resp = GrpcClient.getUserStub().register(req);

        if(1 == resp.getCode()){
            log.info("user {} login", username);
            return Response.success("login success", null);
        }else{
            log.warn("login happen error:{}", resp.getMsg());
            return Response.failed(resp.getMsg());
        }
    }
}
