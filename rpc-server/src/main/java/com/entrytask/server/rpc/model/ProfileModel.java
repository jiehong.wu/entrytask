package com.entrytask.server.rpc.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProfileModel implements Serializable {
    private String password;
    private String nickname;
    private String profile;
}
