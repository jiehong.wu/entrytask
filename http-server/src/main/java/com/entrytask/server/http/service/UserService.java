package com.entrytask.server.http.service;

import com.entrytask.server.http.common.Response;

import javax.servlet.http.HttpServletResponse;

public interface UserService {
    public Response login(String username, String password, HttpServletResponse httpServletResponse);

    public Response register(String username, String password);

}
